import React, { useState } from 'react';

const Search1 = ({ onSearch }) => {
  // constructor(props) {
  //   super(props);
  //   this.state = { 
  //     keyword: "",
  //   };
  // }
  const [keyword, setKeyword] = useState("");

  const onChange = (event) => {
    setKeyword(event.target.value);
  };
  const handleSearch = () => {
    onSearch(keyword);
    // console.log(keyword)
  };

  // render() {
  //   var { keyword } = this.state;
  return (
    <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6">
      <div className="input-group">
        <input
          name="keyword"
          type="text"
          className="form-control"
          placeholder="Nhập từ khóa..."
          value={keyword}
          onChange={onChange}
        />
        <span className="input-group-btn">
          <button
            className="btn btn-primary"
            type="button"
            onClick={handleSearch}
          >
            <span className="fa fa-search mr-5"></span>Tìm
            </button>
        </span>
      </div>
    </div >
  );

};

export default Search1;
