import React, { useState } from 'react';

import TaskItem from "./TaskItem";
const TaskList = ({ tasks, onFilter, onUpdateStatus, onDeleteTask, onSelectedItem }) => {

  const [filterName, setFilterName] = useState("");
  const [filterStatus, setFilterStatus] = useState(-1);


  const onChange = event => {
    const target = event.target;
    const name = target.name;
    const value = target.value;
    onFilter(
      name === "filterName" ? value : filterName,
      name === "filterStatus" ? value : filterStatus
    );
    // this.state({
    //   [name]: value,
    // });
    setFilterName(name === "filterName" ? value : null);
    setFilterStatus(name === "filterStatus" ? value : null);
  };

  // const { tasks } = props; // var tasks = this .props.tasks

  const elmTasks = tasks.map((task, index) => {
    return (
      <TaskItem
        key={task.id}
        task={task}
        index={index + 1}
        onUpdateStatus={onUpdateStatus}
        onDeleteTask={onDeleteTask}
        onSelectedItem={onSelectedItem}
      />
    );
  });
  return (
    <div className="row mt-15">
      <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <table className="table table-bordered table-hover">
          <thead>
            <tr>
              <th className="text-center">STT</th>
              <th className="text-center">Tên</th>
              <th className="text-center">Trạng Thái</th>
              <th className="text-center">Hành Động</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td></td>
              <td>
                <input
                  type="text"
                  className="form-control"
                  name="filterName"
                  value={filterName}
                  onChange={onChange}
                />
              </td>
              <td>
                <select
                  className="form-control"
                  name="filterStatus"
                  value={filterStatus}
                  onChange={onChange}
                >
                  <option value="-1">Tất Cả</option>
                  <option value="0">Ẩn</option>
                  <option value="1">Kích Hoạt</option>
                </select>
              </td>
              <td></td>
            </tr>
            {elmTasks}
          </tbody>
        </table>
      </div>
    </div>
  );
}


export default TaskList;
