import React from 'react';
import Search1 from "./Search1";
import Sort from "./Sort";

function Control(props) {
  const { onSearch, onSort, sortBy, sortValue } = props;
  return (
    <div className="row mt-15">
      <Search1 onSearch={onSearch} />
      <Sort
        onSort={onSort}
        sortBy={sortBy}
        sortValue={sortValue}
      />
    </div>
  );
}


export default Control;
