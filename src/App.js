import React, { useEffect, useState } from "react";
import "./App.css";
import TaskList from "./components/TaskList";
import TaskForm from "./components/TaskFrom";
import Control from "./components/Control";

const App = () => {
  const [tasks, setTasks] = useState([]);
  const [isDisplayForm, setIsDisplayForm] = useState(false);
  const [itemEditing, setItemEditing] = useState(null);
  const [filter, setFilter] = useState(
    {
      name: "",
      status: -1,
    });
  const [keyword, setKeyword] = useState("");
  const [sortBy, setSortBy] = useState("name");
  const [sortValue, setSortValue] = useState(1);
  // state = {
  //   // tasks: [],
  //   // isDisplayForm: false,
  //   // itemEditing: null,
  //   // filter: {
  //   //   name: "",
  //   //   status: -1,
  //   // },
  //   keyword: "",
  //   sortBy: "name",
  //   sortValue: 1,
  // };


  const localData = () => {
    if (localStorage && localStorage.getItem("tasks")) {
      const tasks = JSON.parse(localStorage.getItem("tasks"));
      setTasks(
        tasks
      );
    }
  }

  // didmount
  useEffect(() => {
    localData();
  }, [])

  const s4 = () => {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  }

  const guid = () => {
    return (
      s4() +
      s4() +
      "-" +
      s4() +
      "-" +
      s4() +
      "-" +
      s4() +
      "-" +
      s4() +
      s4() +
      s4()
    );
  }

  const findIndex = (id) => {
    // const { tasks } = setTasks;
    const result = -1;
    tasks.forEach((task, index) => {
      if (task.id === id) {
        result = index;
      }
    });
    return result;
  };

  const onUpdateStatus = (id) => {
    const tasks = setTasks;
    const index = findIndex(id);
    tasks[index].status = !tasks[index].status;
    setTasks(
      tasks
    );
    localStorage.setItem("tasks", JSON.stringify(tasks));
  };

  const onSave = (data) => {
    // const tasks = tasks;
    data.status = data.status === "true" ? true : false;
    if (data.id === "") {
      data.id = guid();
      tasks.push(data);
    } else {
      const index = findIndex(data.id);
      tasks[index] = data;
    }
    setTasks(
      tasks
    );
    localStorage.setItem("tasks", JSON.stringify(tasks));
  };

  const onToggleForm = () => {
    if (itemEditing !== null) {
      setItemEditing(itemEditing)
    } else {
      setIsDisplayForm({
        isDisplayForm: !isDisplayForm,
      });
    }
  };

  const onExitForm = () => {
    setIsDisplayForm(isDisplayForm)
    setItemEditing(itemEditing)

    // this.setState({
    //   isDisplayForm: false,
    //   itemEditing: null,
    // });
  };

  const onDeleteTask = (id) => {
    //  const  { tasks } = this.state;
    const index = findIndex(id);
    tasks.splice(index, 1);
    setTasks(
      tasks
    );
    localStorage.setItem("tasks", JSON.stringify(tasks));
    onExitForm();
  };

  const onSelectedItem = (item) => {
    setItemEditing({ itemEditing: item });
    setIsDisplayForm({ isDisplayForm: true });
    // this.setState({
    //   itemEditing: item,
    //   isDisplayForm: true,
    // });
  };
  const onFilter = (filterName, filterStatus) => {
    filterStatus = parseInt(filterStatus, 10);
    setFilter({

      name: filterName.toLowerCase(),
      status: filterStatus,

    });
  };
  const onSearch = () => {
    setKeyword(
      keyword
    );
  };

  const onSort = () => {
    setSortBy(sortBy);
    setSortValue(sortValue);
    // this.setState({
    //   sortBy: sortBy,
    //   sortValue: sortValue,
    // });
  };

  const {
    tasks,
    isDisplayForm,
    itemEditing,
    filter,
    keyword,
    sortBy,
    sortValue,
  } = this.state;
  if (filter) {
    if (filter.name) {
      tasks = tasks.filter((task) => {
        return task.name.toLowerCase().indexOf(filter.name) !== -1;
      });
    }
    tasks = tasks.filter((task) => {
      if (filter.status === -1) {
        return task;
      } else {
        return task.status === (filter.status === 1 ? true : false);
      }
    });
  }
  if (keyword) {
    tasks = tasks.filter((task) => {
      return task.name.toLowerCase().indexOf(keyword) !== -1;
    });
  }
  var elmForm =
    isDisplayForm === true ? (
      <TaskForm
        onSave={onSave}
        onExitForm={onExitForm}
        itemEditing={itemEditing}
      />
    ) : (
        ""
      );
  if (sortBy === "name") {
    tasks.sort((a, b) => {
      if (a.name > b.name) return sortValue;
      else if (a.name < b.name) return -sortValue;
      else return 0;
    });
  } else {
    tasks.sort((a, b) => {
      if (a.status > b.status) return -sortValue;
      else if (a.status < b.status) return sortValue;
      else return 0;
    });
  }
  return (
    <div className="container">
      <div className="text-center">
        <h1>Quản Lý Công Việc</h1>
        <hr />
      </div>
      <div className="row">
        <div
          className={
            isDisplayForm === true
              ? "col-xs-4 col-sm-4 col-md-4 col-lg-4"
              : ""
          }
        >
          {elmForm}
        </div>
        <div
          className={
            isDisplayForm === true
              ? "col-xs-8 col-sm-8 col-md-8 col-lg-8"
              : "col-xs-12 col-sm-12 col-md-12 col-lg-12"
          }
        >
          <button
            type="button"
            className="btn btn-primary"
            onClick={onToggleForm}
          >
            <span className="fa fa-plus mr-5"></span>Thêm Công Việc
            </button>
          <Control
            onSearch={onSearch}
            onSort={onSort}
            sortBy={sortBy}
            sortValue={sortValue}
            testA={true}
          />
          <TaskList
            tasks={tasks}
            onUpdateStatus={onUpdateStatus}
            onDeleteTask={onDeleteTask}
            onSelectedItem={onSelectedItem}
            onFilter={onFilter}
          />
        </div>
      </div>
    </div>
  );
}


export default App;
